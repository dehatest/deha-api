<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Để tránh lặp đoạn code trả về response
     */
    public function sentSuccessResponse($data = '', $message = '', $status = 200) {
        return \response()->json([
            'data' => $data,
            'message' => $message,
            'status' => $status
        ], $status);
    }
}
