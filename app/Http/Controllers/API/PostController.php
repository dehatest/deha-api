<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostController extends Controller
{
    protected $post;

    public function __construct(Post $post) {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->post->paginate(5);

        //* Trả về Collection 
        $postCollection = new PostCollection($posts);

        return $this->sentSuccessResponse($postCollection, 'success', Response::HTTP_OK);
        /* End trả về Collection */

        /* Trả về resources có links (next, prev, first, last) page
        $postResources = PostResource::collection($posts)->response()->getData(true);

        return response()->json([
            'data' => $postResources
        ], Response::HTTP_OK);
        /* End trả về resources có links (next, prev, first, last) page */
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        // Lấy thông tin cần thay đổi được gửi lên từ client
        // Ví dụ: name, body
        $dataCreate = $request->all();
        // Tạo Post với thông tin lấy được ở bước trên
        $post = $this->post->create($dataCreate);
        // Tạo dữ liệu trả về với thông tin của Post vừa tạo được
        $postResource = new PostResource($post);

        return $this->sentSuccessResponse($postResource, 'success', Response::HTTP_OK); 

        // return response()->json([
        //     'data' => $postResource
        // ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Lấy thông tin Post dựa trên id được truyền lên từ client
        // Nếu không tìm thấy thì trả về message đã cover ở StorePostRequest hàm failedValidation()
        $post = $this->post->findOrFail($id);
        // Tạo dữ liệu trả về với thông tin của Post vừa tạo được
        $postResource = new PostResource($post);

        return $this->sentSuccessResponse($postResource, 'success', Response::HTTP_OK);

        // return response()->json([
        //     'data' => $postResource
        // ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        // Lấy thông tin Post dựa trên id được truyền lên từ client
        $post = $this->post->findOrFail($id);
        // Lấy thông tin cần thay đổi được gửi lên từ client
        // Ví dụ: name, body
        $dataUpdate = $request->all();
        // Update dữ liệu mới
        $post->update($dataUpdate);
        // Tạo dữ liệu trả về với thông tin của Post vừa tạo được
        $postResource = new PostResource($post);

        return $this->sentSuccessResponse($postResource, 'success', Response::HTTP_OK);

        // return response()->json([
        //     'data' => $postResource
        // ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Lấy thông tin Post dựa trên id được truyền lên từ client
        $post = $this->post->findOrFail($id);
        // Xoá Post
        $post->delete();
        // Tạo dữ liệu trả về với thông tin của Post vừa tạo được
        $postResource = new PostResource($post);

        return $this->sentSuccessResponse($postResource, 'Delete successfully!', Response::HTTP_OK);

        // return response()->json([
        //     'data' => $postResource,
        //     'message' => 'Delete successfully!'
        // ], Response::HTTP_OK);
    }
}
