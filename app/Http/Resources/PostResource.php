<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        /* Trả về đầy đủ các thông tin của 1 đối tượng
        return parent::toArray($request);
        /* End trả về đầy đủ các thông tin của 1 đối tượng */

        //* Customize thông tin của đối tượng trả về 
        return [
            'id' => $this->id,
            'name' => $this->name,
            'body' => $this->body 
        ];
        /* End customize thông tin của đối tượng trả về */

    }
}
