<?php

use App\Http\Controllers\API\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*+----------------------------------------------
    METHOD  |          URI        |    NAME     |
-------------------------------------------------
GET|HEAD    | /                   |              
GET|HEAD    | api/posts           | posts.index  
POST        | api/posts           | posts.store  
GET|HEAD    | api/posts/{post}    | posts.show   
PUT|PATCH   | api/posts/{post}    | posts.update 
DELETE      | api/posts/{post}    | posts.destroy
*/
Route::apiResource('posts', PostController::class);
